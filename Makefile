PRJSRC=$(wildcard *.cpp)
EXEC=japper
INSTALLPATH=~/.local/bin
CC=g++
CXXFLAGS=-Wall -std=c++11
OBJDEPS=$(PRJSRC:.cpp=.o)

TXTFILES=$(wildcard *.txt)

all: $(EXEC)

$(EXEC) : $(OBJDEPS)
	$(CC) $(CXXFLAGS) -o $(EXEC) $(OBJDEPS)
%.o: %.cpp
	$(CC) $(CXXFLAGS) -c $<

install: $(EXEC)
	cp *.txt $(INSTALLPATH)
	cp $(EXEC) $(INSTALLPATH)/$(EXEC)
	
clean:
	rm $(EXEC) $(OBJDEPS)
mrproper: clean
	rm $(INSTALLPATH)/$(EXEC)
