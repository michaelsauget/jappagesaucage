#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <string>
#include <ctime>

using namespace std;

vector<string> sauces;
vector<string> verbes;

bool isDuplicate(vector<string>& vect, string s) {
    for(string str : vect) {
        if(str == s) {
            return true;
        }
    }
    return false;
}

void fillVectorFromFile(vector<string>& vect, string fileName) {

    std::ifstream file(fileName);
    if(file.fail()){
        std::cout << "Impossible d'ouvrir le fichier : " << fileName << std::endl;
        return;
    }
    std::string temp;
    while(std::getline(file, temp)) {
        if(!isDuplicate(vect, temp)) {
            vect.push_back(temp);
        }
    }
}

void japperDeLaSauce(const string& shitSauce){
    string command = "say -v Thomas \"" + shitSauce + "\"";
    system(command.c_str());
}

int main() {

    fillVectorFromFile(verbes, "verbesconjug.txt");
    fillVectorFromFile(sauces, "sauces.txt");

    srand(std::time(nullptr)); // use current time as seed for random generator

    string verbe = verbes[rand() % verbes.size()];
//    verbe[0] = toupper(verbe[0]);
    string sauce = sauces[rand() % sauces.size()];

    string shitSauce = "Ta mère " +  verbe + " " + sauce;
    cout << shitSauce << endl;
    japperDeLaSauce(shitSauce);

    return 0;
}
